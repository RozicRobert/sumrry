<?php

namespace App\Http\Controllers;

use App\Summarization;
use Illuminate\Http\Request;

class SummarizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Summarization  $summarization
     * @return \Illuminate\Http\Response
     */
    public function show(Summarization $summarization)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Summarization  $summarization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Summarization $summarization)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Summarization  $summarization
     * @return \Illuminate\Http\Response
     */
    public function destroy(Summarization $summarization)
    {
        //
    }
}
