<?php
namespace App\Http\Controllers;


use App\Summarization;
use Illuminate\Http\Request;
use Storage;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use File;

class SumrryController extends Controller
{



    public function summarize(Request $request)
    {
        $scriptPath = base_path()."/scripts/summarize.py";

        $inputText = $request->input_text;
        $compression = $request->compression ?? 0.2;

        $timestamp = now()->timestamp;

        $inputFile = 'input_' . $timestamp . '.txt';
        $outputFile = 'output_' . $timestamp . '.txt';

        $words_before = str_word_count($inputText);

        // Save file to disk
        Storage::disk('local')->put($inputFile, $request->input_text);
        $inputPath = Storage::disk('local')->path($inputFile);
        $folder = dirname($inputPath);
        $outputPath = $folder . '/' .$outputFile;

        // Convert to unix format
        $dos2unix = new Process(['dos2unix', $inputPath]);
        $dos2unix->run();

        if (!$dos2unix->isSuccessful()) {
            throw new ProcessFailedException($dos2unix);
        }

        // Call python script to summarize text
        $process = new Process(['python3',
            $scriptPath,
            "-i", $inputPath,
            "-o", $outputPath,
            '-c', $compression]);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $summary = File::get($outputPath);
        $words_after = str_word_count($summary);
        $real_compression = (float) $words_after/$words_before;

        Summarization::create([
            'input_file_path' => $inputPath,
            'putput_file_path' => $outputPath,
            'words_before' => $words_before ,
            'words_after' => $words_after,
            'desired_compression' => $compression,
            'real_compression' => $real_compression,
        ]);

        return response()->json([
            'result' => $summary,
            'words_before' => $words_before,
            'words_after' => $words_after,
            'compression' => $real_compression
        ]);
    }
}
