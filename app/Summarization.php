<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Summarization extends Model
{
    protected $fillable = [
        'desired_compression', 'real_compression',
        'words_before', 'words_after'
    ];
}
