import Vue from 'vue'
import Router from 'vue-router'

import Sumrry from './views/Sumrry'
import Result from "./views/Result";

Vue.use(Router);

export default new Router({
    linkActiveClass: 'open active',
    routes: [
        {
            path: '/',
            name: 'Sumrry',
            component: Sumrry
        },
        {
            path: '/result',
            name: 'Result',
            component: Result
        },
/*        {
            path: '/about',
            name: 'About',
            component: About
        },*/
    ]
})
