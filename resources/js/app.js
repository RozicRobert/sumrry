import Vue from 'vue'
import router from './router';

require('./bootstrap');

// Bootstrap vue
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue);

// Axios
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['Accept'] = 'application/json';

import Sumrry from "./views/Sumrry";
import SumitFooter from "./components/SumitFooter"
import SumrryHeader from "./components/SumrryHeader"

new Vue({
    el: '#app',
    router,
    components: {
        SumrryHeader,
        Sumrry,
        SumitFooter,
    }
});


