<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSummarizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summarizations', function (Blueprint $table) {
            $table->id();
            $table->decimal('desired_compression', 10, 9);
            $table->decimal('real_compression', 10, 9);
            $table->integer('words_before');
            $table->integer('words_after');
            $table->string('input_file_path');
            $table->string('output_file_path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summarizations');
    }
}
