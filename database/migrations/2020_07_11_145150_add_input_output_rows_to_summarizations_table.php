<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInputOutputRowsToSummarizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('summarizations', function (Blueprint $table) {
            $table->string('input_file_path')->default('input.txt');
            $table->string('output_file_path')->default('output.txt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('summarizations', function (Blueprint $table) {
            //
        });
    }
}
